package com.zhangyuxuan.springsecurity.service;

import com.zhangyuxuan.springsecurity.bean.UserBean;
import com.zhangyuxuan.springsecurity.util.TestData;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @ClassName AuthService
 * @Description AuthService
 * @Author zhangyuxuan
 * @Date 2025/1/13 19:19
 * @Version 1.0
 */
@Service
public class AuthService {
    private String demoUserName = "admin";
    private String demoUserPass = "admin";
    @Resource
    private TestData testData;

    public UserBean userLogin(UserBean user) {
        UserBean queryUser = testData.queryUser(user);
        if (queryUser != null) {
            queryUser.setUserId(UUID.randomUUID().toString());
        }
        return queryUser;
    }
}