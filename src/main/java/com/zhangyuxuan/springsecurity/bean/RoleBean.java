package com.zhangyuxuan.springsecurity.bean;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName RoleBean
 * @Description RoleBean
 * @Author zhangyuxuan
 * @Date 2025/1/13 18:33
 * @Version 1.0
 */
@Data
public class RoleBean {
    private String roleId;
    private String roleName;
    private List<ResourceBean> roleResources = new ArrayList<>();

    public RoleBean(String roleId, String roleName) {
        this.roleId = roleId;
        this.roleName = roleName;
    }
}