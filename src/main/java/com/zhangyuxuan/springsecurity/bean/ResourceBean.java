package com.zhangyuxuan.springsecurity.bean;

import lombok.Data;

/**
 * @ClassName ResourceBean
 * @Description ResourceBean
 * @Author zhangyuxuan
 * @Date 2025/1/13 18:32
 * @Version 1.0
 */
@Data
public class ResourceBean {
    private String resourceId;
    private String resourceName;
    private String resourceType;

    public ResourceBean(String resourceId, String resourceName) {
        this.resourceId = resourceId;
        this.resourceName = resourceName;
    }
}