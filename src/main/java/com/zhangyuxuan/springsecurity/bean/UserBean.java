package com.zhangyuxuan.springsecurity.bean;

import lombok.Data;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @ClassName UserBean
 * @Description UserBean
 * @Author zhangyuxuan
 * @Date 2025/1/13 18:33
 * @Version 1.0
 */
@Data
public class UserBean {
    private String userId;
    private String userName;
    private String userPass;
    private List<RoleBean> userRoles = new ArrayList<>();
    private Map<String, ResourceBean> userResourceMap;

    public UserBean(String userId, String userName, String userPass) {
        this.userId = userId;
        this.userName = userName;
        this.userPass = userPass;
    }

    public boolean havePermission(String resourceName) {
        if (userResourceMap == null) {
            userResourceMap = userRoles.stream().map(RoleBean::getRoleResources).flatMap(Collection::stream).collect(Collectors.toMap(ResourceBean::getResourceName, Function.identity(), (o1, o2) -> o1));
        }
        return userResourceMap.containsKey(resourceName);
    }
}