package com.zhangyuxuan.springsecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName Controller
 * @Description Controller
 * @Author zhangyuxuan
 * @Date 2025/1/13 19:02
 * @Version 1.0
 */
@RestController
@RequestMapping("/mobile")
public class MobileController {
    /**
     * 代表一个查询手机号的接口
     * @return
     */
    @GetMapping("/query")
    public String query() {
        return "mobile";
    }
}