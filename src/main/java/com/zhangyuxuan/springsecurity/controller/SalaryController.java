package com.zhangyuxuan.springsecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName SalaryController
 * @Description SalaryController
 * @Author zhangyuxuan
 * @Date 2025/1/13 19:09
 * @Version 1.0
 */
@RestController
@RequestMapping("/salary")
public class SalaryController {
    /**
     * 代表一个查询薪水的接口
     * @return
     */
    @GetMapping("/query")
    public String query() {
        return "salary";
    }
}