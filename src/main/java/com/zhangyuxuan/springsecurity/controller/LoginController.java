package com.zhangyuxuan.springsecurity.controller;

import com.zhangyuxuan.springsecurity.bean.UserBean;
import com.zhangyuxuan.springsecurity.service.AuthService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName LoginController
 * @Description LoginController
 * @Author zhangyuxuan
 * @Date 2025/1/13 19:11
 * @Version 1.0
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class LoginController {
    @Resource
    private AuthService authService;

    @PostMapping("/login")
    public String login(UserBean loginUser, HttpServletRequest request) {
        UserBean userBean = authService.userLogin(loginUser);
        if (userBean != null) {

        }
        return null;
    }
}