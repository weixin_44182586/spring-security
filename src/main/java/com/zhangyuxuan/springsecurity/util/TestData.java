package com.zhangyuxuan.springsecurity.util;

import com.zhangyuxuan.springsecurity.bean.ResourceBean;
import com.zhangyuxuan.springsecurity.bean.RoleBean;
import com.zhangyuxuan.springsecurity.bean.UserBean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName TestData
 * @Description TestData
 * @Author zhangyuxuan
 * @Date 2025/1/13 19:24
 * @Version 1.0
 */
@Component
public class TestData {
    private List<UserBean> allUser;

    private List<UserBean> getAllUser() {
        if (allUser == null) {
            allUser = new ArrayList<>();

            ResourceBean mobileResource = new ResourceBean("1", "mobile");
            ResourceBean salaryResource = new ResourceBean("2", "salary");
            List<ResourceBean> adminResources = new ArrayList<>();
            adminResources.add(mobileResource);
            adminResources.add(salaryResource);

            List<ResourceBean> managerResources = new ArrayList<>();
            managerResources.add(salaryResource);

            RoleBean adminRole = new RoleBean("1", "mobile");
            adminRole.setRoleResources(adminResources);
            RoleBean managerRole = new RoleBean("2", "manager");
            managerRole.setRoleResources(managerResources);
            List<RoleBean> adminRoles = new ArrayList<>();
            adminRoles.add(adminRole);
            List<RoleBean> managerRoles = new ArrayList<>();
            managerRoles.add(managerRole);

            UserBean user1 = new UserBean("1", "admin", "admin");
            user1.setUserRoles(adminRoles);
            UserBean user2 = new UserBean("2", "manager", "manager");
            user2.setUserRoles(managerRoles);
            UserBean user3 = new UserBean("3", "worker", "worker");

            allUser.add(user1);
            allUser.add(user2);
            allUser.add(user3);
        }
        return allUser;
    }

    public UserBean queryUser(UserBean user) {
        return null;
    }
}